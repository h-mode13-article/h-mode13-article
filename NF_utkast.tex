\documentclass{article}
\usepackage[a4paper]{geometry}
\usepackage{graphicx}
\usepackage[utf8x]{inputenc}
\usepackage{hyperref}
\usepackage{todonotes}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{upgreek}
\usepackage{fixmath}
\usepackage{lmodern}
\usepackage{subfig}
\usepackage[sort&compress]{natbib}
\bibpunct{[}{]}{,}{n}{}{}

%\renewcommand{\tau}{\uptau}
\renewcommand{\epsilon}{\varepsilon}

\newcommand{\grad}{\nabla}
\newcommand{\Dd}{\ensuremath{\mathrm{d}}}
\newcommand{\unit}[1]{\ensuremath{\,\mathrm{#1}}}
\newcommand{\D}[2]{\ensuremath{\frac{\partial#2}{\partial#1}}}
\renewcommand{\vec}[1]{\ensuremath{\boldsymbol{#1}}} % bold vectors, instead

\newcommand{\figplacing}{hbt}
\newcommand{\figwidth}{0.75\linewidth}
\newcommand{\iu}{\ensuremath{\mathrm{i}}}
\newcommand{\note}[1]{\ensuremath{\mathrm{^#1}}}
\newcommand{\TODO}[1]{\todo[inline]{#1}}

\makeatletter
 \providecommand\phantomcaption{\caption@refstepcounter\@captype}
\makeatother

\title{Particle transport in density gradient driven TE~mode turbulence}
\author{A. Skyman\note{1}, H. Nordman\note{1}, P. I. Strand\note{1}\\
\small \note{1}Euratom--VR Association, Department of Earth and Space Sciences, \\\small Chalmers University of Technology, SE-412 96 Göteborg, Sweden}
\date{}

\begin{document}
%{\LARGE NF Paper: \today}
%
%\listoftodos
%\TODO{remove list of todos in final version!}
%\TODO{spell check}
%\tableofcontents
%\thispagestyle{empty}
%\newpage
%~
%\thispagestyle{empty}
%\cleardoublepage
%\setcounter{page}{1}
%
%\listoffigures

\maketitle
\begin{abstract}
\noindent The turbulent transport of main ion and trace impurities in a tokamak device in the presence of steep electron density gradients has been studied.
The parameters are chosen for trapped electron (TE) mode turbulence, driven primarily by steep electron density gradients relevant to H-mode physics.
Results obtained through non-linear (NL) and quasilinear (QL) gyrokinetic simulations using the GENE code are compared with results obtained from a fluid model.
Impurity transport is studied by examining the balance of convective and diffusive transport, as quantified by the density gradient corresponding to zero particle flux (impurity peaking factor).
Scalings are obtained for the impurity peaking with the background electron density gradient and the impurity charge number.
It is shown that the impurity peaking factor is weakly dependent on impurity charge and significantly smaller than the driving electron density gradient.
\end{abstract}


\section{Introduction} \label{sec:introduction}
\noindent The compatibility between a reactor-grade plasma and the material walls surrounding the plasma is one of the main challenges facing a magnetic fusion device.
The presence of very low levels of high $Z$ impurities in the core plasma may lead to unacceptable levels of radiation losses and fuel dilution.
Also low $Z$ impurities, in the form of beryllium or helium-ash, may result in fuel dilution that severely limits the attainable fusion power~\cite{Harte2010}.
Consequently, the transport properties of impurities is a high priority issue in present experimental and theoretical fusion plasma research.
This is emphasised by the the new ITER-like wall experiment in JET~\cite{Matthews2009}, where a beryllium-clad first wall in the main chamber, combined with carbon and tungsten tiles in the divertor, will be tested for the first time.
 
The transport of main fuel as well as impurities in the core region of tokamaks is expected to be dominated by turbulence driven by Ion Temperature Gradient (ITG) modes and Trapped Electron (TE) modes.
The main drives for the ITG/TE~mode instabilities are gradients of temperature and density combined with unfavourable magnetic curvature.
Most of the theoretical studies of turbulent particle transport have been devoted to temperature gradient driven ITG~and TE~modes, using both fluid, and quasilinear (QL) and nonlinear (NL) gyrokinetic models~\cite{Frojdh1992, Basu2003, Estrada-Mila2005, Naulin2005, Priego2005, Fulop2006, Bourdelle2007, Dubuit2007, Camenen2009, Fulop2010, Futatani2010, Hein2010, Moradi2010, Fulop2011, Nordman2008, Angioni2006, Nordman2007a, Angioni2007, Angioni2009a, Fulop2009, Nordman2011, Skyman2011a}.
Much less effort has been devoted to particle transport in regions with steep density gradients. % based on NL gyrokinetic simulations.
The density gradient, which is stabilising for ITG modes, provides a drive for TE~modes which may dominate the temperature gradient drive for plasma profiles with $R/L_{n_e}>R/L_{T_e}$.
This may occur in connection with the formation of transport barriers, like the high confinement mode (H-mode) edge pedestal, in fusion plasmas.

In the present article, the turbulent transport of main ion and trace impurities in tokamaks is investigated through nonlinear (NL) gyrokinetic simulations using the GENE code.\footnote{\url{http://www.ipp.mpg.de/~fsj/gene/}} 
The main part considers collisionless TE~modes driven by density gradients.
The impurity density gradient for zero impurity flux is calculated for varying background electron density gradient drive and for a range of impurity species.
This study complements recent studies~\cite{Nordman2011, Skyman2011a} on temperature gradient driven TE~and ITG~mode impurity transport. 
The NL~GENE results are compared with QL~gyrokinetic simulations and a computationally efficient multi fluid model, suitable for use in predictive transport simulations.
Of particular interest is the sign of the impurity convective flux and the degree of impurity peaking in the presence of strong background electron density gradients.

The remainder of the article is structured as follows:
in section~\ref{sec:fluid} impurity transport is briefly reviewed, with emphasis on topics relevant to the study;
this is followed by section~\ref{sec:results} on the simulations and a discussion of the main results.
The article concludes with section~\ref{sec:conclusions}, containing a summary of the main conclusions to be drawn.


\section{Transport models} \label{sec:gyro} \label{sec:fluid}
%\subsection{Gyrokinetics} 
The models used have been described in detail elsewhere, see~\cite{Nordman2011} and references therein, only a brief summary is given here.

The NL and QL GENE simulations were performed in a flux tube geometry, in a low $\beta$ ($\beta=10^{-4}$) s--$\alpha$ equilibrium~\cite{Jenko2000, Dannert2005a, Dannert2005b, Merz2008a}.
The simulations include gyrokinetic electrons (passing and trapped), and gyrokinetic main ions and impurities.
Effects of finite $\beta$, plasma shaping, equilibrium $\vec{E}\times\vec{B}$ flow shear and collisions have been neglected.
The effects of collisions are known to be important for the turbulent fluctuation and transport levels~\cite{Angioni2009b}, however, their effects on the impurity peaking factor have been shown to be small~\cite{Fulop2010}.
In order to ensure that the resolution was adequate, the resolution was varied separately for the perpendicular, parallel and velocity space coordinates, and the effects of this on the mode structure, $k_\perp$ spectra and flux levels were investigated. 
The resolution was then set sufficiently high for the effects on these indicators to have converged.
For a typical NL simulation for main ions, fully kinetic electrons, and one trace species, a resolution of $n_{x} \times n_{y} \times n_{z} = 96 \times 96 \times 24$ grid points in real space and of $n_{v} \times n_{\mu} = 48 \times 12$ in velocity space was chosen.
For QL~GENE simulations the box size was set to $n_{x} \times n_{y} \times n_{z} = 8 \times 1 \times 24$  and $n_{v} \times n_{\mu} = 64 \times 12$ respectively.
The impurities were included self-consistently as a third species in the simulations, with the trace impurity particle density $n_Z/n_e = 10^{-6}$ in order to ensure that they have a negligible effect on the turbulence.

%In quasilinear mode only one of the most unstable modes is captured at a time, not the interaction between dominant and sub-dominant modes, and only for the particular length scale $k_\theta\rho_s$ of choice. 
%If the length scale is chosen appropriately, however, the quasilinear simulation will capture the essential features of the transport mechanism, and it is useful for getting a qualitative understanding of the physical processes. 
%\cite{Dannert2005a, Dannert2005b, Merz2008a}
%
%

%\section{Impurity transport} \label{sec:transport}
%In kinetic theory the plasma is described through distribution functions of velocity and position for each of the included plasma species.  
%Hence, kinetic equations are inherently six-dimensional, however, in magnetically confined fusion plasmas the confined particles are generally constrained to tight orbits along field lines.  
%This motivates averaging over the gyration, reducing the problem to five-dimensional gyrokinetic equations.\cite{Antonsen1980,Frieman1982,Hahm1988,Brizard1989}
%Since the equations governing the evolution of the distributions are all coupled, the resulting decrease in numerical complexity is considerable.
%
%Fluid theory, on the other hand, is derived by taking the moments of the kinetic equations to some order, making them tractable by finding an appropriate closure.\cite{Weiland2000}
%In addition to making the letterings of the plasma more accessible, by reintroducing familiar physical concepts such as pressure and density, fluid models are also several orders of magnitude more computationally efficient. 


%The Weiland fluid model~\cite{Weiland2000}
%\todo{more references neeed?}
%is used to derive the impurity density response as
%
%\begin{equation} \label{eq:n_phi}
% \widetilde{n}_Z = \left[\widetilde{\omega}\left(\frac{R}{2 L_{n_Z}}
%-\lambda\right) - 
% \tau_Z^*\left(\frac{R}{2 L_{T_Z}} - \frac{7}{3}\frac{R}{2 L_{n_Z}} +
%\frac{5 \lambda}{3}\right) + 
% \frac{Z}{A_Z q_*^2}\left(\frac{\widetilde{\omega} +
%5 \tau_Z^*/3}{\widetilde{\omega}- 2 \tau_Z^*}\right)
% \right]\frac{\widetilde{\phi}}{N}, 
%\end{equation}
%
%\noindent where
%
%\begin{equation} \label{eq:N}
% N=\widetilde{\omega}^2 + \frac{10 \tau_Z^*}{3}\widetilde{\omega} + \frac{5 \tau_Z^{*2}}{3}.
%\end{equation}

For the fluid simulations, the Weiland multi-fluid model~\cite{Weiland2000} is used to derive the main ion, impurity, and trapped electron density response from the corresponding fluid equations in the collisionless and electrostatic limit.
The fluid simulations include first order Finite Larmor-Radius (FLR) effects for the main ions, and parallel main ion/impurity dynamics.
The free electrons are assumed to be Boltzmann distributed.
The equations are closed by the assumption of quasineutrality:

\begin{equation} \label{eq:qneutrality}
 \frac{\delta n_e}{n_e} = \left(1 - Z f_Z\right) \frac{\delta n_i}{n_i} + Z f_Z \frac{\delta n_Z}{n_Z},
\end{equation}

\noindent where $f_Z = n_Z/n_e$ is the fraction of impurities with charge $Z$, and $n_j$ and $\delta n_j$ are the density and the density perturbation for species $j$.
An eigenvalue equation for TE~and ITG~modes is thus obtained in the presence of impurities.
A strongly ballooning eigenfunction with $k_\parallel^2=\left(3 q^2 R^2\right)^{-1}$ valid for magnetic shear $s \sim 1$ is used~\cite{Hirose1994}.
The eigenvalue equation is then reduced to a system of algebraic equations that is solved numerically.
%Alternatively, the eigenvalue equation can be solved for general mode widths~\cite{Weiland2000}.


%\subsubsection{Impurity transport} 
\label{sec:PF}
The main ion and impurity particle fluxes can then be written as:

\begin{equation} \label{eq:Gamma_derivation}
\Gamma_{j} =
\left<\delta n_j v_E\right> = 
-n_j \rho_s c_s\left<\widetilde{n}_j\frac{1}{r}\D{\theta}{\widetilde{\phi}}
\right>.
\end{equation}

\noindent Here $v_E$ is the radial $\vec{E}\times\vec{B}$ drift velocity, $\rho_s=c_s/\Omega_{ci}$ is ion sound scale, with $c_s=\sqrt{T_e/m_i}$ being the ion sound speed and $\Omega_{ci} = e B/m$ the ion cyclotron frequency.
On the right hand side, the perturbations in density and electrostatic potential are defined $\widetilde{n}_j=\delta n_j/n_j$ and $\widetilde{\phi} = e \phi/T_e$ respectively.
The angled brackets in equation~\eqref{eq:Gamma_derivation} imply a time and space average over all unstable modes. 
Performing this averaging, the particle flux can be written:

%\begin{multline} \label{eq:transport}
% \frac{\Gamma_{Z}}{n_Z  c_s} =
%\frac{k_\theta \rho_s \widetilde{\gamma}\left|\widetilde{\phi}_k\right|^2}{
%\left|N\right|^2}
% \Biggl\{\left.
% \frac{R}{2 L_{n_Z}}\left(\left|\widetilde{\omega}\right|^2 +
%\frac{14 \tau_Z^*}{3} \widetilde{\omega}_r + \frac{55 \tau_Z^{*2}}{9}\right)-
% \right.\\
% \frac{R}{2 L_{T_Z}}\left(2 \tau_Z^* \widetilde{\omega}_r +
%\frac{10 \tau_Z^{*2}}{3}\right)-
% \left<\lambda\right>\left(\left|\widetilde{\omega}\right|^2 +
%\frac{10 \tau_Z^*}{3} \widetilde{\omega}_r + \frac{35 \tau_Z^{*2}}{9}\right)+
% \\ \left. 
% \frac{Z}{3 A_Z q_*^2 \left|N_1\right|^2}
% \left[ \tau_Z^*\left(
% \frac{19}{3} \widetilde{\omega}_r^2 - \frac{1}{3} \widetilde{\gamma}^2 +
%\frac{100 \tau_Z^*}{3} \widetilde{\omega}_r -5 \tau_Z^{*2}
% \right) + 2 \widetilde{\omega}_r\left|\widetilde{\omega}\right|^2 \right]
% \right.\Biggr\},
%\end{multline}

\begin{equation}
 \label{eq:transport}
 \frac{R\Gamma_j}{n_j} = D_j\frac{R}{L_{n_j}} + D_{T_j}\frac{R}{L_{T_j}} + R V_{p,j}.
\end{equation}


%The first term in Eq.~\eqref{eq:transport} corresponds to the diffusive part of Eq.~\eqref{eq:transport_short}, whereas the three subsequent terms correspond to the convective part of the transport of the impurity species. 
%Of this, the $R/L_{T_Z}$ term is the thermodiffusion, the sign of which is governed mainly by the real frequency, $\widetilde{\omega}_r$. 
%For TE~modes, $\widetilde{\omega}_r>0$, resulting the thermodiffusion generally giving an inward pinch for TE~modes.
%Due to the $Z$-dependence in $\tau_Z^*$, this term scales as $V_Z^{\grad T}\sim (1/Z)(R/L_{T_Z})$ to leading order, rendering it unimportant for large $Z$ impurity species, but it is important for lighter elements, such as the Helium ash.
%Further, the $\left<\lambda\right>$ term gives the curvature pinch, which is usually inward, and the final term is the parallel compression term for the impurities.
%As opposed to the thermodiffusion, the parallel compression pinch is usually outward for TE~modes.
%Its $Z$ dependence is $V_Z^\parallel\sim Z/A_Z k_\parallel^2\sim Z/A_Z q^2$, but since $A_Z\approx2 Z$ this is is expected to be a very weak scaling.
%For ITG modes, $\widetilde{\omega}_r<0$, meaning that the influence of the terms discussed above is reversed.\cite{Angioni2006}

The first term in equation~\eqref{eq:transport} corresponds to diffusion, the second to the thermodiffusion and the third to the convective velocity (pinch), where $1/L_{n_j}=-\grad n_j/n_j$, $n_j$ is the density of species $j$ and $R$ is the major radius of the tokamak.
The pinch contains contributions from curvature and parallel compression effects.
The terms of equation~\eqref{eq:transport} have been described in detail in previous work~\cite{Nordman2007a, Nordman2008, Nordman2011, Angioni2006}.
For trace impurities, equation~\eqref{eq:transport} can be uniquely written 

\begin{equation}
 \label{eq:transport_short}
 \frac{R\Gamma_Z}{n_Z} = D_Z\frac{R}{L_{n_Z}} + R V_Z,
\end{equation}

\noindent where $D_Z$ is the impurity diffusion coefficient and $V_Z$ is the total impurity convective velocity with the thermodiffusive term included, and neither $D_Z$ nor $V_Z$ depend on $1/L_{n_Z}$.
The sign of the thermodiffusive, or ``thermopinch'', term is decided mainly by the real frequency, $\widetilde{\omega}_r$. 
For electron modes $\widetilde{\omega}_r=R\omega_r/c_s<0$, resulting in the thermodiffusion generally giving an inward contribution to the pinch for TE~modes.
For an impurity with charge number $Z$, this term scales as $D_{T_Z}\sim (1/Z)(R/L_{T_Z})$ to leading order, rendering it unimportant for large $Z$ impurity species, but it is important for lighter elements, such as the He ash.

The zero-flux impurity density gradient (peaking factor) is defined as $PF_Z=-RV_Z/D_Z$ for the value of the impurity density gradient that gives zero impurity flux.
The peaking factor thus quantifies the balance between convective and diffusive impurity transport.
Solving the linearised equation~\eqref{eq:transport_short} for $R/L_{n_Z}$ with $\Gamma_Z = 0$ yields the interpretation of $PF_Z$ as the gradient of zero impurity flux.
It is found by first computing the impurity particle flux $\Gamma_Z$ for values of $R/L_{n_Z}$ in the vicinity of $\Gamma_Z=0$.
The diffusivity ($D_Z$) and convective velocity ($V_Z$) are then given by fitting the acquired fluxes to equation~\eqref{eq:transport_short}, whereafter the peaking factor is obtained through their quotient. 
This is illustrated in figure~\ref{fig:Gamma_Z}.

%The main ion flux, on the other hand, does not exhibit a linear dependence on the density gradient.
%Therefore, the background peaking was obtained by varying $R/L_{n_{i,e}}$ in small steps, until approximately zero main ion flux was obtained.

%\section{Simulations} \label{sec:simulations}
%\TODO{Sanity check and maybe shorten -- too similar to PoP!}
%As has been show in previous studies CITERA,\cite{Angioni2006,Nordman2011} the transport of impurities is dependent on the impurity charge. 
%This is also evident from Eq.~\eqref{eq:transport}.
%In this article, the transport of impurities has been studied numerically, by calculating the impurity peaking factor ($PF_Z$) for impurities with various impurity charge ($Z$) and varying values of the driving background desnity gradient. 
%The process of calculating the peaking factor is illustrated in figure~\ref{fig:Gamma_Z}.
%The impurity particle flux $\Gamma_Z$ is computed for $\grad n_Z$ in the vicinity of $\Gamma_Z=0$, taking the estimated residuals of the samples' uncertainty into account. 
%The diffusivity $D_Z$ and convective velocity $RV_Z$ are then given by fitting the acquired fluxes to Eq.~\eqref{eq:transport_short}, where after the peaking factor is obtained as $PF_Z=-\frac{RV_Z}{D_Z}$ (see section~\ref{sec:PF}).
%
%The instabilities causing the transport are fuelled by the free energy present in gradients in the system, and in general the steeper the gradient the more free energy is available, which is expected to lead to stronger modes and more pronounced transport.
%Two families of gradients are available that can drive the instabilities: 
%the temperature gradients ($-R\grad T_j/T_j\approx R/L_{T_j}$) and the density gradients ($-R\grad n_j/n_j\approx R/L_{n_j}$), where $j=i,\,e$ for main ions and electrons respectively.\footnote{the density and temperature gradients of the impurity species can also drive turbulent transport, however, for trace amounts this effect is negligible}
%Numerical studies have been performed, focused on the dependence of the impurity peaking factor on the background density gradient.

\section{Simulation results} \label{sec:results}
%\subsection{Main ion flux}
The main parameters used in the simulations are summarised in table~\ref{tab:parameters}.
The parameters where chosen to represent an arbitrary tokamak geometry at about mid radius, and do not represent any one particular experiment.
A moderately steep electron temperature gradient ($R/L_{T_e}=5.0$) together with a flatter ion temperature gradient ($R/L_{T_i, Z}=2.0$) was used to promote TE~mode dominated dynamics.
Following~\cite{Ernst2009}, the background density gradient for the base scenario was set higher than the temperature gradient, to ensure density gradient driven dynamics.
In order to preserve quasineutrality $\grad n_e=\grad n_i$ was used. 
The quasilinear gyrokinetic and fluid results were calculated for a single poloidal mode number with $k_\theta \rho_s=0.2$; see~\cite{Nordman2011} for a discussion on the choice of $k_\theta \rho_s$.

First, the main ion particle flux ($\Gamma_p$) is studied.
Time averaged fluxes are calculated from time series of NL~GENE data after convergence, as illustrated in figure~\ref{fig:time_series}.
The scalings of $\Gamma_p$ with the electron density gradient obtained from NL~GENE and fluid simulations are shown in figure~\ref{fig:Gamma_p}.
%The large transport found in NL~GENE simulations is an indication of the stiffness of the gyrokinetic model, and is often seen in fixed-gradient simulations of turbulence.
%\todo{Referee 2.7\ldots cite? skriv om... (se svar)}
The fluid and nonlinear gyrokinetic model show similar scalings for the main ion flux, but the gyrokinetic transport exhibits a stronger dependence on $R/L_{n_e}$.
This is in line with the trends seen for the linear eigenvalues, as show in figure~\ref{fig:omn_TEM_eigens}.
%The main ion density gradient corresponding to zero ion flux ($PF_p$) can be found by similar means to that of $PF_Z$, however, since the trace approximation is not valid for the main ions, the zero-flux gradient has to be found explicitly by varying $\grad n_p$ until the condition $\Gamma_p=0$ is satisfied.
The NL~GENE and fluid results presented in figure~\ref{fig:Gamma_p} indicate that for the present parameters, neither model gives a main ion flux reversal for TE~mode driven turbulence.


%\subsection{Scaling with $R/L_{n_e}$}
Next, the scaling of the impurity transport with the background density gradient ($R/L_{n_e}$) is investigated.
The results for the impurity peaking factor are shown in figure~\ref{fig:omn_TEM}.
We note that the impurity peaking increases with $R/L_{n_e}$, saturating at $PF_Z\approx 2.0$ for large values of the electron density gradient.
The QL~GENE results tend to consistently overestimate the peaking factors compared to the NL~GENE results, while the fluid model gives results that are somewhat below the NL~GENE results for the steeper gradients.
The fluid results show a considerably less dramatic dependency of the peaking factor than the QL and NL gyrokinetic results, both of which show a strong decrease in $PF_Z$ as the electron density profiles flatten.
This is observed for all values of the impurity charge number.
As the background density profile becomes more peaked, a corresponding increase in impurity transport is expected.
This is illustrated in figure~\ref{fig:D_RV}, where scalings, obtained from NL~GENE simulations, of the diffusivity ($D_Z$) and convective velocity ($R V_Z$) with $R/L_{n_e}$ are shown.
Although the magnitudes of $D_Z$ and $R V_Z$ both show a strong increase with $R/L_{n_e}$, in accordance with the scaling of the growth rate seen in figure~\ref{fig:omn_TEM_eigens}, the impurity peaking ($PF_Z=-RV_Z/D_Z$) is only weakly sensitive to the electron density gradient.
For $R/L_{n_e}\lesssim 2.0$ the impurity peaking factor is not well defined, since both $D_Z$ and $R V_Z$ go to zero.
The fluid and gyrokinetic results are in qualitative agreement, showing a growth rate that increases uniformly with $R/L_{n_e}$.
For the studied parameters, there are no clear signs of a transition from density gradient driven to temperature gradient driven TE~mode turbulence, which has been reported to dominate for $R/L_{n_e} \lesssim R/L_{T_e}$~\cite{Ernst2009}.

%\subsection{Scaling with $Z$}
The scaling of the impurity peaking factor with impurity charge ($Z$), with $R/L_{n_e}$ as a parameter, is illustrated in figure~\ref{fig:Z}.
The impurity charge was varied from $Z=2$ (He) to $Z=42$ (Mo).
The models show only a very weak scaling, with $PF_Z$ falling toward saturation for higher $Z$.
The results are similar to those for the temperature gradient driven TE~mode reported in~\cite{Skyman2011a}.
Notably, the QL~GENE simulations overestimate the peaking factors compared to the NL~GENE results, whereas the fluid results are lower than the NL~GENE results.
The scalings observed for low $Z$ impurities ($Z\lesssim 10$) is weak or reversed compared to results for the ITG~mode driven case, reported in e.g.~\cite{Nordman2011}, where a strong rise in $PF_Z$ with increasing $Z$ was obtained.
The qualitative difference between the TE~and the ITG~mode dominated cases can be understood from the $Z$-dependent thermodiffusion in equation~\eqref{eq:transport}, which is outward for ITG~modes and inward for TE~modes.

%\subsection{The odd one out}
%Since $PF_Z$ is much lower than the driving $R/L_{n_e}$, the simulations required a correspondingly smaller $R/L_{n_Z}$.
%An observed effect of this, was that the $k_\perp$ spectra of the impurities were shifted toward larger $k$.
%This is illustrated in figure~\ref{fig:kspectra}.
%As the effect was persistent, despite changes in resolution, this is not thought to be a purely numerical artifact.
%
%This observation meant that, in order to investigate systems with trace species in steep driving gradients, precautions had to be taken to ensure that the trace dynamics were properly resolved.
%
%The $k_\perp$ spectra of the impurities show a shift toward larger $k$, when $R/L_{n_Z} < R/L_{n_e}$.
%The trend is investigated by measuring the quotient between the mean $k_y$ of impurity and main ions as a function of $(R/L_{n_Z})/(R/L_{n_e})$.
%The results are shown in figure~\ref{fig:kratios}, where it the shift in $\left<k\right>$ can be seen.


\section{Conclusions} \label{sec:conclusions}
In summary, the turbulent transport of main ion and trace impurities in regions of steep density gradients has been investigated through nonlinear (NL) gyrokinetic simulations using the GENE code.
The simulations included gyrokinetic electrons (passing and trapped), and gyrokinetic main ions and impurities in a low $\beta$ $s$--$\alpha$ equilibrium.
The main part has considered collisionless TE~modes driven by steep density gradients, a parameter regime of relevance for the formation of transport barriers in fusion plasmas.
The NL~GENE results for the density gradient of zero impurity particle flux (peaking factor) have been compared with QL kinetic simulations and a reduced and computationally efficient multi-fluid model, suitable for use in predictive transport simulations.
In the simulations, the magnetic shear and safety factor were held fixed at $s=0.8$ and $q=1.4$.
For the parameters studied, qualitative agreement between gyrokinetic and fluid results has been obtained for the scaling of the impurity peaking factor with both the background density gradient and the impurity charge.
%For the $R/L_{n_e}$ scaling, a strong increase in $PF_Z$ was observed in the gyrokinetic simulations, as the gradient steepened. 
An inward impurity convective velocity, corresponding to positive peaking factor, was found in all cases considered.
In the region of steep electron density gradients, it was shown that the impurity peaking factor saturates at values significantly smaller than the driving electron density gradient.
%For the fluid model, the trend was similar but not as pronounced.
In general, a good qualitative agreement between the considered models was found.
It was, however, noted that for the chosen length scales ($k_\theta \rho_s=0.2$), the QL~GENE, in comparison with the NL~GENE results, tended to overestimate the peaking factors, whereas the fluid results were close to or lower than the NL~GENE results.
The scaling of the peaking factor with impurity charge was observed to be weak, with a slight increase in the impurity peaking factor observed in the gyrokinetic results for low impurity charge numbers.
%The observed QL trend can be understood from the $Z$ dependence of the thermopinch contribution to the convective velocity (equation~\eqref{eq:transport}).


%In the future, we aim to seek to shed new light on new and similar situations.
%To go, boldly, where some men have gone before.

\section*{Acknowledgements}
The simulations were performed on resources provided on the Lindgren\footnote{\url{http://www.pdc.kth.se/resources/computers/lindgren/}} and HPC-FF\footnote{\url{http://www2.fz-juelich.de/jsc/juropa/}} high performance computers, by the Swedish National Infrastructure for Computing (SNIC) at Paralleldatorcentrum (PDC) and the European Fusion Development Agreement (EFDA), respectively.

The authors would like to thank Frank Jenko, Tobias G\"orler, M.~J. P\"uschel, and the rest of the GENE~team at IPP--Garching for their help with the gyrokinetic simulations.


%\bibliographystyle{unsrtnat}
\bibliographystyle{iopart-num}
\bibliography{fusion.bib}

\clearpage

\begin{table}[\figplacing]
 \centering
% \scriptsize
 \caption[Parameters]{\small Parameters used in the fluid and gyrokinetic simulations, \note{\dag} denotes scan parameters}
 \label{tab:parameters}
 \begin{tabular}{l||r|r}\hline
 & $R/L_{n_e}$-scaling: & $Z$-scaling: \\ \hline\hline
 $T_i/T_e$:             & $1.0$     & $1.0$     \\
 $s$:                   & $0.8$     & $0.8$     \\
 $q$:                   & $1.4$     & $1.4$     \\
 $\beta$:               & $10^{-4}$ & $10^{-4}$ \\
 $\epsilon=r/R$:        & $0.14$    & $0.14$    \\
 $k_\theta \rho_s$:      & $0.2$     & $0.2$     \\
 $n_e$, $n_i+Z\,n_Z$:      & $1.0$     & $1.0$  \\ %& $10^{19}$\unit{m^{-3}}\\
 $n_Z$ \emph{(trace)}:  & $10^{-6}$ & $10^{-6}$ \\
% $R$:                   & $1.0$   & $1.0$\\ %&\unit{m}\\
 $R/L_{T_i},R/L_{T_Z}$:             & $2.0$         & $2.0$ \\
 $R/L_{T_e}$:                       & $5.0$         & $5.0$ \\ %\hline
 $R/L_{n_{i,e}}$:\note{\dag}        & $1.0$--$13.0$ & $5.0$--$13.0$ \\
 $Z$:\note{\dag}                    & $2$, $28$     & $2$--$42$ 
% $\left(l_x,\, l_y\right)$:  & $\left(2\pi,\, 2\pi/k\rho\right)\cdot\rho$ & $\left(2\pi,\, 2\pi/k\rho\right)\cdot\rho$ \\
% $N_x\times N_{ky}\times N_z$: & $8\times1\times24$ & $8\times1\times24$\\
% $N_{v_{||}}\times N_\mu$:     & $64\times12$ & $64\times12$
 \end{tabular}
\end{table}

\clearpage

\begin{figure}[\figplacing]
 \centering
 \includegraphics[width=\figwidth]{figures/Gamma}
 \caption[Impurity flux $\left(\Gamma_Z\right)$]{Impurity flux $\left(\Gamma_Z\right)$ as a function of the impurity density gradient $\left(-R\grad n_Z/n_Z=R/L_{n_Z}\right)$, illustrating the process of finding the impurity peaking factor ($PF_Z$), diffusivity ($D_Z$) and convective velocity ($V_Z$).
NL GENE data of TEM turbulence in a proton plasma with He impurities, and background density gradient $R/L_{n_e} = 5.0$.}
 \label{fig:Gamma_Z}
\end{figure}
\clearpage

\begin{figure}[\figplacing]
 \centering
 \subfloat[time series and time averages of the main ion flux $\left(\Gamma_p\right)$ from NL~GENE simulations \label{fig:time_series}]
{\includegraphics[width=\figwidth]{figures/timeseries}}
\phantomcaption{}
\end{figure}

\begin{figure}[\figplacing]
 \ContinuedFloat
 \centering
 \subfloat[main ion flux $\left(\Gamma_p\right)$ dependence on the background density gradient ($R/L_{n_e}$). 
\label{fig:Gamma_p}]
{\includegraphics[width=\figwidth]{figures/flux_levels}}
\phantomcaption{}
\end{figure}

\begin{figure}[\figplacing]
 \ContinuedFloat
 \centering
 \subfloat[scaling of real frequency ($\omega_r$) and growth rate ($\gamma$) with the background density gradient \label{fig:omn_TEM_eigens}]
{\includegraphics[width=\figwidth]{figures/omn_TEM_eigens}}
 \caption{Main ion flux $\left(\Gamma_p\right)$ dependence on the background electron density gradient ($-R\grad n_e/n_e=R/L_{n_e}$). 
NL~GENE and fluid data with protons as main ions.
Parameters are $q=1.4$, $s=0.8$, $\epsilon=r/R=0.14$, $R/L_{T_i, Z}=2.0$, $R/L_{T_e}=5.0$, and $\tau=T_e/T_i=1.0$. 
The fluid data was obtained for $k_\theta \rho_s = 0.2$. 
The fluxes are normalised to $v_{T,i}n_e\rho_i^2/R^2$. 
The error bars indicate an estimated uncertainty of one standard deviation.
The eigenvalues in figure~\ref{fig:omn_TEM_eigens} are from fluid and GENE simulations, and are normalised to $c_s/R$.}
\label{fig:main_ions}
\end{figure}

\clearpage

\begin{figure}[\figplacing]
 \centering
 \subfloat[dependence of the impurity peaking factor $\left(PF_Z\right)$ on the background density gradient \label{fig:omn_TEM}]
{\includegraphics[width=\figwidth]{figures/omn_TEM}}
\phantomcaption{}
\end{figure}

\begin{figure}[\figplacing]
 \ContinuedFloat
 \centering
 \subfloat[dependence of the impurity diffusivity and convective velocity ($D_Z$ and $RV_Z$) on the background density gradient \label{fig:D_RV}]
{\includegraphics[width=\figwidth]{figures/omn_TEM_D_RV}}
\caption{Scalings of the impurity peaking factor ($PF_Z=-RV_Z/D_Z$) with the background electron density gradient ($R/L_{n_e}$), with parameters as in figure~\ref{fig:main_ions}.
QL and fluid data have been acquired using $k_\theta\rho_s=0.2$.
Figure~\ref{fig:D_RV} shows the diffusivities and pinches corresponding to the NL~GENE impurity peaking factors ($PF_Z$) in figure~\ref{fig:omn_TEM}.
$D_Z$ and $RV_Z$ are normalised to $v_{T,i}\rho_i^2/R$.
The error bars indicate an estimated uncertainty of one standard deviation.}
\label{fig:omn}
\end{figure}
\clearpage

\begin{figure}[\figplacing]
 \centering
 \includegraphics[width=\figwidth]{figures/Z_TEM}
 \caption{Scaling of the impurity peaking factor ($PF_Z=-RV_Z/D_Z$) with impurity charge $Z$, with parameters as in figure~\ref{fig:main_ions}; $k_\theta\rho_s=0.2$ was used in the QL and fluid simulations.
The error bars indicate an estimated uncertainty of one standard deviation.}
 \label{fig:Z}
\end{figure}
\clearpage

%\begin{figure}[\figplacing]
% \centering
% \includegraphics[width=\figwidth]{figures/kratios}
% \caption[$-\grad n_Z \sim R/L_{n_Z}$ scaling of the ratio $k_y^Z/k_y^p$]{$-\grad n_Z \sim R/L_{n_Z}$ scaling of the ratio between mean and maximum wave numbers for impurities and main ions ($Z$ and $p$ respectively) $\left(k_{y,Z}/k_{y,p}\right)$. 
%NL GENE data of TEM turbulence with protons as main ions.
%$R/L_{n_Z}=1.0,2.0,4.0,6.0,8.0,10.0,16.0$, $R/L_{n_p}=8.0$
%}
% \label{fig:kratios}
%\end{figure}
%\clearpage
%
%\begin{figure}[\figplacing]
% \centering
% \includegraphics[width=\figwidth]{figures/kspectra_same}
% \caption[$k_y$ spectra for $Z$ and $p$ with $R/L_{n_Z} = R/L_{n_p}$]{$k_y$ spectra for impurities and main ions ($Z$ and $p$ respectively) with $R/L_{n_Z} = R/L_{n_p} = 8.0$. 
%NL GENE data of TEM turbulence with protons as main ions.}
% \label{fig:kspect_same_omn}
%\end{figure}
%
%\begin{figure}[\figplacing]
% \centering
% \includegraphics[width=\figwidth]{figures/kspectra_diff}
% \caption[$k_y$ spectra for $Z$ and $p$ with $R/L_{n_Z} \ll R/L_{n_p}$]{$k_y$ spectra for impurities and main ions ($Z$ and $p$ respectively) with $R/L_{n_Z} = 1.0$ and $R/L_{n_p} = 8.0$. 
%NL GENE data of TEM turbulence with protons as main ions.}
% \label{fig:kspect_diff_omn}
%\end{figure}
%\clearpage

\end{document}
